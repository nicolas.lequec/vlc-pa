# VLC Plot Analyzer

This repository provides tools to debug the timestamps and events from
the core of VLC:

- vlc_pa.py: stdin/http log parser to Matplotlib visualizer.

The `vlc_pa.py` script can be used with files using `cat`:

```
cat log_traces.txt | python vlc_pa.py
```

Likewise, it can be used with the (unmerged) HTTP logger from VLC for
live updates on the PTS values.

```
python vlc_pa.py --http
```
