
var configURL = '/config.json';

var resources = []
var data = {datasets: []}
var labels = []
var deriv_label = []
var labelNames = []
var myChart = null
var newData = false

var defaultPointRadius = 2.5
var defaultLineWidth = 1.5

var keepOnlyLastValues = false
var lastSeconds = 0.0

// full data store
var dataMap = {}

function initData() {
  for (resource of resources) {
    label = resource[0]
    labels.push(label)
  }
  for (derivative of derivatives) {
    label = derivative[0]
    deriv_label.push(label)
  }
  createTableStats()
}

function addCurve(name, yAxis) {
  let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16)
  labelNames.push(name)
  dataMap[name] = []
  
  // Add in the dataset
  data.datasets.push({
    label: name,
    data: dataMap[name],
    backgroundColor: randomColor,
    borderColor: randomColor,
    showLine: true,
    lineTension: 0,
    fill: false,
    yAxisID: yAxis,
    pointRadius: defaultPointRadius,
    borderWidth: defaultLineWidth,
  })

  console.log("Adding plot: " + name)

  // Add in the table stat
  let row = table.insertRow()
  let cell = row.insertCell()
  let text = document.createTextNode(name)
  cell.appendChild(text)
  cell = row.insertCell()
  cell.id = name + "Points"
  text = document.createTextNode("0")
  cell.appendChild(text)

  console.log("Adding stat cell: " + name)
}

/* Check if the function exist and add it if not */
function checkFunctionExistence(name, data_set, index, attribute) {
  ii = labelNames.indexOf(name)
  if (ii == -1) {
    // Add a new curb
    if (data_set[index][1].includes(attribute))
      addCurve(name, 'y')
    else if (data_set[index][2].includes(attribute))
      addCurve(name, 'y2')
    else
      console.log(name)
  }
}

/* Add a new value to the function at the index places in the resources array */
function updateFunction(index, msg) {
  for (v of resources[index][1].concat(resources[index][2])) {
    if (v in msg) {
      var name = msg.label + " " + v

      checkFunctionExistence(name, resources, index, v)

      //myChart.data.datasets[ii].data.push({ x: msg.timestamp, y: msg[v]})
      if (keepOnlyLastValues) {
        cutDataFrom(name, msg.timestamp - (lastSeconds * 1000))
      }
      dataMap[name].push({ x: msg.timestamp, y: msg[v]})
      updateTableStat(name)
    }
  }
}

/* Calculate a new value to the derivative of the function
 * at the index places in the derivatives array */
function updateDerivative(index, msg) {
  for (v of derivatives[index][1].concat(derivatives[index][2])) {
    if (v in msg) {
      var function_name = msg.label + " " + v
      var deriv_name = msg.label + " derivative " + v

      checkFunctionExistence(deriv_name, derivatives, index, v)

      //myChart.data.datasets[ii].data.push({ x: msg.timestamp, y: msg[v]})
      if (keepOnlyLastValues) {
        cutDataFrom(name, msg.timestamp - (lastSeconds * 1000))
      }
      var n = dataMap[function_name].length
      if (n > 1) {
        var deriv_val = (msg[v] - dataMap[function_name][n-2].y) /
                        (msg.timestamp - dataMap[function_name][n-2].x)
        dataMap[deriv_name].push({ x: msg.timestamp, y: deriv_val})
        updateTableStat(deriv_name)
      }
    }
  }
}

fetch(configURL).then(res => res.json()).then((json) => {
  resources = json.resources
  derivatives = json.derivatives

  initData()

  // chartjs initial config
  var config = {
    type: 'scatter',
    data: data,
    options: {
      animation: false,
      scales: {
        x: {
          type: 'linear',
          position: 'bottom'
        },
        y: {
          type: 'linear',
          position: 'left',
          ticks: {
            color: '#ff0000'
          }
        },
        y2: {
          type: 'linear',
          position: 'right',
          ticks: {
            color: '#0000ff'
          },
          grid: {
            drawOnChartArea: false
          }
        }
      },
      plugins: {
        zoom: {
          //pan: {
          //  enabled: true,
          //},
          zoom: {
            wheel: {
              enabled: true,
            },
            drag: {
              enabled: true,
            },
          }
        },
        annotation: {
          annotations: {
          }
        },
        legend: {
          position: "left",
          align: "start",
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                let label = context.dataset.label
                label += ": (" + String(context.parsed.x) + " , " + String(context.parsed.y) + ")"
                return label
            }
          }
        }
      },
      transitions: {
        zoom: {
          animation: {
            duration: 0
          }
        }
      },
    }
  }

  myChart = new Chart(
    document.getElementById('myChart'),
    config
  )

  if ("WebSocket" in window) {
    var ws = new WebSocket("ws://localhost:8080/ws");
    ws.onopen = function() {
      //ws.send("data");
    };
    ws.onmessage = function(evt) {
      var msg = JSON.parse(evt.data)
      var i = labels.indexOf(msg.config_label)
      var i_deriv = deriv_label.indexOf(msg.config_label)

      if (i == -1) { return }

      updateFunction(i, msg)
      if (i_deriv >= 0) {
        updateDerivative(i_deriv, msg)
      }

      newData = true

      //myChart.update()
      //ws.send("data");
    };
    ws.onclose = function() {
      console.log("Connection is closed...");
    };
  } else {
    console.log("WebSocket NOT supported by your Browser!");
  }

}).catch(err => { throw err })

// Update chart at max 10fps
var intervalId = setInterval(function() {
  if (newData) {
    newData = false
    myChart.update()
  }
}, 100);

function addVerticalLine(position, label) {
  config.options.plugins.annotation.annotations[label] = {
    type: 'line',
    xMin: position,
    xMax: position,
    borderColor: 'rgb(255, 99, 132)',
    borderWidth: 1,
    borderDash: [10, 10],
    label: {
      enabled: true,
      content: label,
      position: "start",
      rotation: 90
    }
  }
  myChart.update()
}

function clipData(){
  let min = parseInt(document.getElementById("min").value);
  if (isNaN(min)) { min = 0 }
  let max = parseInt(document.getElementById("max").value);
  console.log("Min: " + min)
  console.log("Max: " + max)
  myChart.data.datasets.forEach(function(dataset) {
    let name = dataset.label
    let sliceStart = dataMap[name].findIndex((point) => { return point.x >= min })
    if (sliceStart == -1) { sliceStart = 0 }
    if (isNaN(max)) { max = dataset.data.length }
    let sliceEnd = dataMap[name].findIndex((point) => { return point.x > max })
    if (sliceEnd == -1) { sliceEnd = dataset.data.length }
    console.log("sliceStart: " + sliceStart)
    console.log("sliceEnd: " + sliceEnd)
    dataset.data = dataMap[name].slice(sliceStart, sliceEnd)
  })
  myChart.update()
}

function cutDataFrom(name, startTime) {
  let cutStart = dataMap[name].findIndex((point) => { return point.x >= startTime })
  if (cutStart == -1) { return }
  dataMap[name].splice(0, cutStart)
}

function resetDataClip(){
  myChart.data.datasets.forEach(function(dataset) {
    let name = dataset.label
    dataset.data = dataMap[name]
  })
  myChart.update()
}

function clipDataFromZoom() {
  document.getElementById("min").value = myChart.scales.x.options.min
  document.getElementById("max").value = myChart.scales.x.options.max
  clipData()
}


function createTableStats(){
  table = document.getElementById("stats")
  // create header
  let thead = table.createTHead()
  let row = thead.insertRow()
  let th = document.createElement("th")
  let text = document.createTextNode("Label")
  th.appendChild(text)
  row.appendChild(th)
  th = document.createElement("th")
  text = document.createTextNode("Points")
  th.appendChild(text)
  row.appendChild(th)
  // Generate rows
  for (name in dataMap) {
    let row = table.insertRow()
    let cell = row.insertCell()
    let text = document.createTextNode(name)
    cell.appendChild(text)
    cell = row.insertCell()
    cell.id = name + "Points"
    text = document.createTextNode("0")
    cell.appendChild(text)
  }
}

function updateTableStat(name){
  let tableCell = document.getElementById(name + "Points")
  tableCell.textContent = String(dataMap[name].length)
}

function togglePoints() {
  myChart.data.datasets.forEach(function(dataset) {
    if (dataset.pointRadius == 0) {
      dataset.pointRadius = defaultPointRadius
    } else {
      dataset.pointRadius = 0
    }
  })
  myChart.update()
}

function toggleLines() {
  myChart.data.datasets.forEach(function(dataset) {
    dataset.showLine = !dataset.showLine 
  })
  myChart.update()
}

function showAllDatasets() {
  for (let i = 0; i < myChart.data.datasets.length; i++) {
    myChart.show(i)
  }
}

function hideAllDatasets() {
  for (let i = 0; i < myChart.data.datasets.length; i++) {
    myChart.hide(i)
  }
}

function toggleKeepOnlyLastValues() {
  lastSeconds = parseInt(document.getElementById("lastSeconds").value)
  keepOnlyLastValues = document.getElementById("checkKeepOnly").checked
}

function toggleSidebar() {
  let side = document.getElementById("sideCol")
  side.hidden = !side.hidden
  resizeMainCol()
}

function toggleStats() {
  let stats = document.getElementById("statsCol")
  stats.hidden = !stats.hidden
  resizeMainCol()
}

function resizeMainCol() {
  let side = document.getElementById("sideCol")
  let stats = document.getElementById("statsCol")
  let main = document.getElementById('mainCol')
  if (side.hidden && stats.hidden) { main.className = "col-11" }
  else if (!side.hidden && !stats.hidden) { main.className = "col-6" }
  else { main.className = "col-9" }
}
